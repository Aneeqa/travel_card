package com.aneeqa.travelCard.domain;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "rates")
public class Rate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "source_stop", nullable = false)
    private String source;

    @Column(name = "destination_stop", nullable = false)
    private String destination;

    @Column(name = "rate", nullable = false)
    private BigDecimal charge;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date firstCreatedDateTime;

    @Column(nullable = false)
    private Date lastUpdatedDateTime;

    @Column(nullable = false)
    private String lastUpdatedUser;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public BigDecimal getCharge() {
        return charge;
    }

    public void setCharge(BigDecimal rateAmount) {
        this.charge = rateAmount;
    }

    public Date getFirstCreatedDateTime() {
        return firstCreatedDateTime;
    }

    public void setFirstCreatedDateTime(Date firstCreatedDateTime) {
        this.firstCreatedDateTime = firstCreatedDateTime;
    }

    public Date getLastUpdatedDateTime() {
        return lastUpdatedDateTime;
    }

    public void setLastUpdatedDateTime(Date lastUpdatedDateTime) {
        this.lastUpdatedDateTime = lastUpdatedDateTime;
    }

    public String getLastUpdatedUser() {
        return lastUpdatedUser;
    }

    public void setLastUpdatedUser(String lastUpdatedUser) {
        this.lastUpdatedUser = lastUpdatedUser;
    }

    public Rate() {
    }

    public Rate(Integer id, String source, String destination, BigDecimal charge, Date firstCreatedDateTime,
        Date lastUpdatedDateTime, String lastUpdatedUser) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.charge = charge;
        this.firstCreatedDateTime = firstCreatedDateTime;
        this.lastUpdatedDateTime = lastUpdatedDateTime;
        this.lastUpdatedUser = lastUpdatedUser;
    }

    @Override
    public String toString() {
        return "Rate{" + "id=" + id + ", source=" + source + ", destination=" + destination + ", rate=" + charge
            + ", firstCreatedDateTime=" + firstCreatedDateTime + ", lastUpdatedDateTime=" + lastUpdatedDateTime
            + ", lastUpdatedUser='" + lastUpdatedUser + '\'' + '}';
    }
}
