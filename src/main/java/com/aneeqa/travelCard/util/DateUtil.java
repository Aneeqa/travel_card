package com.aneeqa.travelCard.util;

import java.util.Date;

public class DateUtil {

    public static Long findDuration(Date date1, Date date2) {
        if (date1 != null && date2 != null) {
            return (date2.getTime() - date1.getTime()) / 1000;
        }
        return null;
    }
}
