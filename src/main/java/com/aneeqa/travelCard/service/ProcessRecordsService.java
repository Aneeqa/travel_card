package com.aneeqa.travelCard.service;

import com.aneeqa.travelCard.model.TapModel;
import com.aneeqa.travelCard.model.TransactionType;
import com.aneeqa.travelCard.model.TripModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ProcessRecordsService {

    @Autowired
    private FileUtilService fileUtilService;

    @Autowired
    private TripService tripService;

    @Value("${travelcard.input.file.location}")
    private String inputFile;

    @Value("${travelcard.output.file.location}")
    private String outputFile;

    private Logger logger = LoggerFactory.getLogger(ProcessRecordsService.class);

    public void process() {

        //Read Record
        List<TapModel> taps = fileUtilService.readFile(inputFile);

        //Couldn't read file
        if (taps == null) {
            return;
        }

        //Empty file
        if (taps.isEmpty()) {
            logger.error("No data in file.");
            return;
        }

        Collections.sort(taps, Comparator.comparing(TapModel::getPan)
                                         .thenComparing(TapModel::getTapDateTime));
        //Calculate charges
        List<TripModel> trips = new ArrayList<>();
        while (taps.size() > 1) {
            TapModel current = taps.get(0);
            TapModel next = taps.get(1);

            TripModel trip = tripService.createTrip(current, next);
            taps.remove(current);
            if (trip != null) {
                trips.add(trip);
                if (trip.getStatus() != TransactionType.INCOMPLETE) {
                    taps.remove(next);
                }
            } else {
                logger.error("Error occurred in processing record {}", current.toString());
            }
        }

        if (taps.size() == 1) {
            //Incomplete trip
            TripModel trip = tripService.createTrip(taps.get(0), null);
            if (trip != null) {
                trips.add(trip);
            } else {
                //No data to write
                logger.error("Error occurred in processing record {}", taps.get(0).toString());
                return;
            }
        }

        //Write output file
        if (!trips.isEmpty()) {
            fileUtilService.writeFile(outputFile, trips);
        }

    }

}
