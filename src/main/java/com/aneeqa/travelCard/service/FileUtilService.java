package com.aneeqa.travelCard.service;

import com.aneeqa.travelCard.model.TapModel;
import com.aneeqa.travelCard.model.TripModel;
import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class FileUtilService {

    Logger logger = LoggerFactory.getLogger(FileUtilService.class);

    public List<TapModel> readFile(String filePath) {

        if (!StringUtils.hasText(filePath)) {
            logger.error("Empty file path passed.");
            return null;
        }
        
        List<TapModel> taps = null;
        Reader reader = null;
        try {
            URL url = ClassLoader.getSystemResource(filePath);
            if (url != null) {
                reader = Files.newBufferedReader(Paths.get(url.toURI()));
                taps = new CsvToBeanBuilder(reader).withType(TapModel.class)
                                                   .withIgnoreLeadingWhiteSpace(true)
                                                   .build()
                                                   .parse();
            } else {
                logger.error("Couldn't find file {}", filePath);
            }
        } catch (URISyntaxException | IOException e) {
            logger.error("Couldn't read file {}", filePath);
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return taps;
    }

    public void writeFile(String filePath, List<TripModel> trips) {
        Writer writer = null;
        try {
            Path path = Paths.get(ClassLoader.getSystemResource(filePath)
                                             .toURI());
            writer = new FileWriter(path.toString());

            CustomMappingStrategy<TripModel> mappingStrategy = new CustomMappingStrategy<TripModel>();
            mappingStrategy.setType(TripModel.class);

            StatefulBeanToCsv sbc = new StatefulBeanToCsvBuilder(writer).withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                                                                        .withMappingStrategy(mappingStrategy)
                                                                        .build();

            sbc.write(trips);
        } catch (CsvRequiredFieldEmptyException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            logger.error("Invalid file path.");
            e.printStackTrace();
        } catch (CsvDataTypeMismatchException e) {
            logger.error("Invalid data found.");
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("Couldn't write records to file.");
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
