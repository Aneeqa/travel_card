package com.aneeqa.travelCard.service;

import com.aneeqa.travelCard.domain.Rate;
import com.aneeqa.travelCard.model.TapModel;
import com.aneeqa.travelCard.model.TapType;
import com.aneeqa.travelCard.model.TransactionType;
import com.aneeqa.travelCard.model.TripModel;
import com.aneeqa.travelCard.repository.RateRepository;
import com.aneeqa.travelCard.util.DateUtil;
import java.math.BigDecimal;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TripService {

    @Autowired
    private RateRepository rateRepository;

    private Logger logger = LoggerFactory.getLogger(TripService.class);

    public TripModel createTrip(TapModel current, TapModel next) {

        if (current == null) {
            logger.error("Current tap record passed as null.");
            return null;
        }

        if (next != null) {
            if (current.getPan().equals(next.getPan())
                && current.getCompanyId().equals(next.getCompanyId())
                && current.getBusId().equals(next.getBusId())
                && current.getTapType() == TapType.ON) {

                if (!current.getStopId().equals(next.getStopId())) {
                    if (next.getTapType() == TapType.OFF) {
                        //Complete trip
                        TripModel trip = populateTripDetails(current, next);
                        trip.setChargeAmount(calculateCharge(current.getStopId(), next.getStopId()));
                        trip.setStatus(TransactionType.COMPLETE);
                        return trip;
                    }
                } else {
                    //Cancelled trip
                    TripModel trip = populateTripDetails(current, next);
                    trip.setChargeAmount(BigDecimal.ZERO);
                    trip.setStatus(TransactionType.CANCELLED);
                    return trip;
                }
            }
        }
        //All other scenarios will be incomplete trips
        return populateIncompleteTripDetails(current);

    }

    private TripModel populateTripDetails(TapModel current, TapModel next) {
        TripModel trip = new TripModel();
        Date started = current.getTapDateTime();
        Date finished = next.getTapDateTime();
        trip.setStarted(started);
        trip.setFinished(finished);
        trip.setDurationSeconds(DateUtil.findDuration(started, finished));

        trip.setFromStopId(current.getStopId());
        trip.setToStopId(next.getStopId());

        trip.setCompanyId(current.getCompanyId());
        trip.setBusId(current.getBusId());
        trip.setPan(current.getPan());
        return trip;
    }

    private TripModel populateIncompleteTripDetails(TapModel current) {
        TripModel trip = new TripModel();
        Date started = current.getTapDateTime();
        trip.setStarted(started);
        trip.setFinished(null);
        trip.setDurationSeconds(null);

        trip.setFromStopId(current.getStopId());
        trip.setToStopId(null);

        trip.setCompanyId(current.getCompanyId());
        trip.setBusId(current.getBusId());
        trip.setPan(current.getPan());

        trip.setChargeAmount(calculateMaxCharge(current.getStopId()));
        trip.setStatus(TransactionType.INCOMPLETE);

        return trip;
    }

    private BigDecimal calculateCharge(String stop1, String stop2) {
        Rate rate = rateRepository.findBySourceAndDestination(stop1, stop2);
        if (rate == null) {
            rate = rateRepository.findBySourceAndDestination(stop2, stop1);
        }
        if (rate == null) {
            logger.error("Could not find charge between stops {} and {}", stop1, stop2);
            return null;
        } else {
            return rate.getCharge();
        }
    }

    private BigDecimal calculateMaxCharge(String currentStop) {
        Rate rate = rateRepository.findFirstBySourceOrDestinationOrderByChargeDesc(currentStop, currentStop);
        if (rate == null) {
            logger.error("Could not find max charge for stop {}", currentStop);
            return null;
        } else {
            return rate.getCharge();
        }
    }
}
