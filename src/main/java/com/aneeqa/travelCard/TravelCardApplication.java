package com.aneeqa.travelCard;

import com.aneeqa.travelCard.service.ProcessRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TravelCardApplication implements CommandLineRunner {

    @Autowired
    private ProcessRecordsService processRecordsService;

    public static void main(String[] args) {
        SpringApplication.run(TravelCardApplication.class, args);
    }

    @Override
    public void run(String... args) {
        processRecordsService.process();
    }

}
