package com.aneeqa.travelCard.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;
import java.math.BigDecimal;
import java.util.Date;

public class TripModel {

    @CsvDate(value = "DD-MM-YYYY hh:mm:ss")
    @CsvBindByName(column = "Started")
    @CsvBindByPosition(position = 0)
    private Date started;

    @CsvDate(value = "DD-MM-YYYY hh:mm:ss")
    @CsvBindByName(column = "Finished")
    @CsvBindByPosition(position = 1)
    private Date finished;

    @CsvBindByName(column = "DurationSecs")
    @CsvBindByPosition(position = 2)
    private Long durationSeconds;

    @CsvBindByName(column = "FromStopId")
    @CsvBindByPosition(position = 3)
    private String fromStopId;

    @CsvBindByName(column = "ToStopId")
    @CsvBindByPosition(position = 4)
    private String toStopId;

    @CsvBindByName(column = "ChargeAmount")
    @CsvBindByPosition(position = 5)
    private BigDecimal chargeAmount;

    @CsvBindByName(column = "CompanyId")
    @CsvBindByPosition(position = 6)
    private String companyId;

    @CsvBindByName(column = "BusId")
    @CsvBindByPosition(position = 7)
    private String busId;

    @CsvBindByName(column = "PAN")
    @CsvBindByPosition(position = 8)
    private Long pan;

    @CsvBindByName(column = "Status")
    @CsvBindByPosition(position = 9)
    private TransactionType status;

    public Date getStarted() {
        return started;
    }

    public void setStarted(Date started) {
        this.started = started;
    }

    public Date getFinished() {
        return finished;
    }

    public void setFinished(Date finished) {
        this.finished = finished;
    }

    public Long getDurationSeconds() {
        return durationSeconds;
    }

    public void setDurationSeconds(Long durationSeconds) {
        this.durationSeconds = durationSeconds;
    }

    public String getFromStopId() {
        return fromStopId;
    }

    public void setFromStopId(String fromStopId) {
        this.fromStopId = fromStopId;
    }

    public String getToStopId() {
        return toStopId;
    }

    public void setToStopId(String toStopId) {
        this.toStopId = toStopId;
    }

    public BigDecimal getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(BigDecimal chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getBusId() {
        return busId;
    }

    public void setBusId(String busId) {
        this.busId = busId;
    }

    public Long getPan() {
        return pan;
    }

    public void setPan(long pan) {
        this.pan = pan;
    }

    public TransactionType getStatus() {
        return status;
    }

    public void setStatus(TransactionType status) {
        this.status = status;
    }

    public TripModel() {
    }

    public TripModel(Date started, Date finished, Long durationSeconds, String fromStopId, String toStopId,
        BigDecimal chargeAmount, String companyId, String busId, Long pan, TransactionType status) {
        this.started = started;
        this.finished = finished;
        this.durationSeconds = durationSeconds;
        this.fromStopId = fromStopId;
        this.toStopId = toStopId;
        this.chargeAmount = chargeAmount;
        this.companyId = companyId;
        this.busId = busId;
        this.pan = pan;
        this.status = status;
    }

    @Override
    public String toString() {
        return "TripModel{" + "started=" + started + ", finished=" + finished + ", durationSeconds=" + durationSeconds
            + ", fromStopId='" + fromStopId + '\'' + ", toStopId='" + toStopId + '\'' + ", chargeAmount=" + chargeAmount
            + ", companyId='" + companyId + '\'' + ", busId='" + busId + '\'' + ", pan=" + pan + ", status=" + status
            + '}';
    }
}
