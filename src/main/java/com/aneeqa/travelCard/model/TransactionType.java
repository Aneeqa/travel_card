package com.aneeqa.travelCard.model;

public enum TransactionType {
    COMPLETE("COMPLETE"), INCOMPLETE("INCOMPLETE"), CANCELLED("CANCELLED");


    public final String value;

    TransactionType(String value) {
        this.value = value;
    }

    public static TransactionType findByValue(String tapType) {
        for (TransactionType v : values()) {
            if (v.value.equals(tapType)) {
                return v;
            }
        } return null;
    }
}
