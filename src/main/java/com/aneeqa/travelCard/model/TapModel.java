package com.aneeqa.travelCard.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;
import java.util.Date;

public class TapModel {

	@CsvBindByName(column = "ID")
	private int id;

	@CsvDate(value = "DD-MM-YYYY hh:mm:ss")
	@CsvBindByName(column = "DateTimeUTC")
	private Date tapDateTime;

	@CsvBindByName(column = "TapType")
	private TapType tapType;

	@CsvBindByName(column = "StopId")
	private String stopId;

	@CsvBindByName(column = "CompanyId")
	private String companyId;

	@CsvBindByName(column = "BusId")
	private String busId;

	@CsvBindByName(column = "PAN")
	private Long pan;

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getTapDateTime() {
		return tapDateTime;
	}

	public void setTapDateTime(Date tapDateTime) {
		this.tapDateTime = tapDateTime;
	}

	public TapType getTapType() {
		return tapType;
	}

	public void setTapType(TapType tapType) {
		this.tapType = tapType;
	}

	public String getStopId() {
		return stopId;
	}

	public void setStopId(String stopId) {
		this.stopId = stopId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getBusId() {
		return busId;
	}

	public void setBusId(String busId) {
		this.busId = busId;
	}

	public Long getPan() {
		return pan;
	}

	public void setPan(Long pan) {
		this.pan = pan;
	}

	public TapModel(int id, Date tapDateTime, TapType tapType, String stopId, String companyId, String busId,
		Long pan) {
		this.id = id;
		this.tapDateTime = tapDateTime;
		this.tapType = tapType;
		this.stopId = stopId;
		this.companyId = companyId;
		this.busId = busId;
		this.pan = pan;
	}

	public TapModel() {
	}

	@Override
	public String toString() {
		return "TapModel{" + "id=" + id + ", tapDateTime=" + tapDateTime + ", tapType=" + tapType + ", stopId='"
			+ stopId + '\'' + ", companyId='" + companyId + '\'' + ", busId='" + busId + '\'' + ", pan=" + pan + '}';
	}
}
