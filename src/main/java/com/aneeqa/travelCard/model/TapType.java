package com.aneeqa.travelCard.model;

public enum TapType {
    ON("ON"),
    OFF("OFF");

    public final String value;

    TapType(String value) {
        this.value = value;
    }

    public static TapType findByValue(String tapType) {
        for (TapType v : values()) {
            if (v.value.equals(tapType)) {
                return v;
            }
        }
        return null;
    }

}
