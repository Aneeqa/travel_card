package com.aneeqa.travelCard.repository;

import com.aneeqa.travelCard.domain.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RateRepository extends JpaRepository<Rate, Integer> {

    Rate findBySourceAndDestination(String stop1, String stop2);

    Rate findFirstBySourceOrDestinationOrderByChargeDesc(String stop1, String stop2);
}
