package com.aneeqa.travelCard.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import com.aneeqa.travelCard.model.TapModel;
import com.aneeqa.travelCard.model.TapType;
import com.aneeqa.travelCard.model.TransactionType;
import com.aneeqa.travelCard.model.TripModel;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import nl.altindag.log.LogCaptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ProcessRecordsServiceTest {

    @InjectMocks
    private ProcessRecordsService processRecordsService;

    @Mock
    private FileUtilService fileUtilService;

    @Mock
    private TripService tripService;

    private LogCaptor logCaptor = LogCaptor.forClass(ProcessRecordsService.class);

    private static final String NO_DATA_ERROR = "No data in file.";
    private static final String TRIP_CREATION_ERROR = "Error occurred in processing record";

    private static final String STOP_ID = "Stop1";
    private static final String COMPANY_ID = "Company1";
    private static final String BUS_ID = "Bus1";
    private static final Long PAN1 = Long.valueOf("5500005555555558");
    private static final Long PAN2 = Long.valueOf("5500005555555559");

    @Before
    public void setUp() {
        logCaptor.clearLogs();
    }

    @Test
    public void testProcess_ErrorReadingFile() {
        when(fileUtilService.readFile(any())).thenReturn(null);

        processRecordsService.process();

        verify(fileUtilService).readFile(any());
        verifyNoInteractions(tripService);
        verify(fileUtilService, times(0)).writeFile(anyString(), anyList());
        assertTrue(logCaptor.getErrorLogs().isEmpty());
    }

    @Test
    public void testProcess_blankFile() {
        List<TapModel> taps = new ArrayList<>();
        when(fileUtilService.readFile(any())).thenReturn(taps);

        processRecordsService.process();

        verify(fileUtilService).readFile(any());
        verifyNoInteractions(tripService);
        verify(fileUtilService, times(0)).writeFile(any(), any());
        assertFalse(logCaptor.getErrorLogs().isEmpty());
        assertTrue(logCaptor.getErrorLogs().size() == 1);
        assertEquals(NO_DATA_ERROR, logCaptor.getErrorLogs().get(0));
    }

    @Test
    public void testProcess_oneRecord() {
        List<TapModel> taps = new ArrayList<>();
        TapModel tap =
            new TapModel(1, new Date(), TapType.ON, STOP_ID, COMPANY_ID, BUS_ID, PAN1);
        taps.add(tap);

        TripModel tripModel =
            new TripModel(new Date(), new Date(), Long.valueOf("4"), STOP_ID, null, BigDecimal.TEN, COMPANY_ID, BUS_ID,
                PAN1, TransactionType.INCOMPLETE);
        List<TripModel> trips = new ArrayList<>();
        trips.add(tripModel);

        when(fileUtilService.readFile(any())).thenReturn(taps);
        when(tripService.createTrip(tap, null)).thenReturn(tripModel);

        processRecordsService.process();

        verify(fileUtilService).readFile(any());
        verify(tripService, times(1)).createTrip(tap, null);
        verify(fileUtilService).writeFile(any(), eq(trips));
        assertTrue(logCaptor.getErrorLogs().isEmpty());
    }

    @Test
    public void testProcess_MultipleRecord() {
        List<TapModel> taps = new ArrayList<>();
        TapModel tap1 = new TapModel(1, new Date(), TapType.ON, STOP_ID, COMPANY_ID, BUS_ID, PAN1);
        taps.add(tap1);

        TapModel tap2 = new TapModel(2, new Date(), TapType.ON, STOP_ID, COMPANY_ID, BUS_ID, PAN2);
        taps.add(tap2);

        TapModel tap3 = new TapModel(3, new Date(), TapType.OFF, STOP_ID, COMPANY_ID, BUS_ID, PAN1);
        taps.add(tap3);

        TripModel tripModel1 =
            new TripModel(new Date(), new Date(), Long.valueOf("4"), STOP_ID, null, BigDecimal.TEN, COMPANY_ID, BUS_ID,
                PAN1, TransactionType.COMPLETE);
        TripModel tripModel2 =
            new TripModel(new Date(), new Date(), Long.valueOf("4"), STOP_ID, null, BigDecimal.TEN, COMPANY_ID, BUS_ID,
                PAN2, TransactionType.INCOMPLETE);

        List<TripModel> trips = new ArrayList<>();
        trips.add(tripModel1);
        trips.add(tripModel2);

        when(fileUtilService.readFile(any())).thenReturn(taps);
        when(tripService.createTrip(tap1, tap3)).thenReturn(tripModel1);
        when(tripService.createTrip(tap2, null)).thenReturn(tripModel2);

        processRecordsService.process();

        verify(fileUtilService).readFile(any());
        verify(tripService).createTrip(tap1, tap3);
        verify(tripService).createTrip(tap2, null);
        verify(fileUtilService).writeFile(any(), eq(trips));
        assertTrue(logCaptor.getErrorLogs().isEmpty());
    }

    @Test
    public void testProcess_oneRecordNullTrip() {
        List<TapModel> taps = new ArrayList<>();
        TapModel tap =
            new TapModel(1, new Date(), TapType.ON, STOP_ID, COMPANY_ID, BUS_ID, PAN1);
        taps.add(tap);

        when(fileUtilService.readFile(any())).thenReturn(taps);
        when(tripService.createTrip(tap, null)).thenReturn(null);

        processRecordsService.process();

        verify(fileUtilService).readFile(any());
        verify(tripService, times(1)).createTrip(tap, null);
        verify(fileUtilService, times(0)).writeFile(any(), any());
        assertFalse(logCaptor.getErrorLogs().isEmpty());
        assertTrue(logCaptor.getErrorLogs().size() == 1);
        assertEquals(TRIP_CREATION_ERROR + " " + tap.toString(), logCaptor.getErrorLogs().get(0));
    }

    @Test
    public void testProcess_MultipleRecordNullTrips() {
        List<TapModel> taps = new ArrayList<>();
        TapModel tap1 = new TapModel(1, new Date(), TapType.ON, STOP_ID, COMPANY_ID, BUS_ID, PAN1);
        taps.add(tap1);

        TapModel tap2 = new TapModel(2, new Date(), TapType.ON, STOP_ID, COMPANY_ID, BUS_ID, PAN2);
        taps.add(tap2);

        TapModel tap3 = new TapModel(3, new Date(), TapType.OFF, STOP_ID, COMPANY_ID, BUS_ID, PAN1);
        taps.add(tap3);

        when(fileUtilService.readFile(any())).thenReturn(taps);
        when(tripService.createTrip(tap1, tap3)).thenReturn(null);
        when(tripService.createTrip(tap3, tap2)).thenReturn(null);
        when(tripService.createTrip(tap2, null)).thenReturn(null);

        processRecordsService.process();

        verify(fileUtilService).readFile(any());
        verify(tripService).createTrip(tap1, tap3);
        verify(tripService).createTrip(tap2, null);
        verify(fileUtilService, times(0)).writeFile(any(), any());
        assertFalse(logCaptor.getErrorLogs().isEmpty());
        assertTrue(logCaptor.getErrorLogs().size() == 3);
        assertEquals(TRIP_CREATION_ERROR + " " + tap1.toString(), logCaptor.getErrorLogs().get(0));
        assertEquals(TRIP_CREATION_ERROR + " " + tap3.toString(), logCaptor.getErrorLogs().get(1));
        assertEquals(TRIP_CREATION_ERROR + " " + tap2.toString(), logCaptor.getErrorLogs().get(2));
    }

}
