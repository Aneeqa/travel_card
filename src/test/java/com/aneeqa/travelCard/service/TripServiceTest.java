package com.aneeqa.travelCard.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import com.aneeqa.travelCard.domain.Rate;
import com.aneeqa.travelCard.model.TapModel;
import com.aneeqa.travelCard.model.TapType;
import com.aneeqa.travelCard.model.TransactionType;
import com.aneeqa.travelCard.model.TripModel;
import com.aneeqa.travelCard.repository.RateRepository;
import com.aneeqa.travelCard.util.DateUtil;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import nl.altindag.log.LogCaptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TripServiceTest {

	@InjectMocks
	private TripService tripService;

	@Mock
	private RateRepository rateRepository;

	private LogCaptor logCaptor = LogCaptor.forClass(TripService.class);

	private static final String NULL_RECORD_PASSED = "Current tap record passed as null.";

	private static final String STOP_ID1 = "Stop1";
	private static final String STOP_ID2 = "Stop2";
	private static final String COMPANY_ID = "Company1";
	private static final String BUS_ID = "Bus1";
	private static final Long PAN1 = Long.valueOf("5500005555555558");
	private static final Long PAN2 = Long.valueOf("5500005555555559");

	private Rate rate;
	private Date date1;
	private Date date2;
	private Long duration;

	@Before
	public void setUp() throws ParseException {
		logCaptor.clearLogs();

		rate = new Rate();
		rate.setId(1);
		rate.setSource("Stop1");
		rate.setDestination("Stop2");
		rate.setCharge(BigDecimal.TEN);

		DateFormat sdf = new SimpleDateFormat("DD-MM-YYYY hh:mm:ss");
		String dateString1 = "20-01-2020 01:10:25";
		date1 = sdf.parse(dateString1);
		String dateString2 = "20-01-2020 01:10:45";
		date2 = sdf.parse(dateString2);
		duration = DateUtil.findDuration(date1, date2);
	}

	@Test
	public void testCreateTrip_NullStops() {
		assertNull(tripService.createTrip(null, null));
		assertFalse(logCaptor.getErrorLogs().isEmpty());
		assertTrue(logCaptor.getErrorLogs().size() == 1);
		assertEquals(NULL_RECORD_PASSED, logCaptor.getErrorLogs().get(0));
	}

	@Test
	public void testCreateTrip_NullDestination_IncompleteTrip() {
		TapModel tap =
			new TapModel(1, date1, TapType.ON, STOP_ID1, COMPANY_ID, BUS_ID, PAN1);

		when(rateRepository.findFirstBySourceOrDestinationOrderByChargeDesc(STOP_ID1, STOP_ID1))
			.thenReturn(rate);

		TripModel trip = tripService.createTrip(tap, null);

		verify(rateRepository).findFirstBySourceOrDestinationOrderByChargeDesc(STOP_ID1, STOP_ID1);

		assertNotNull(trip);
		assertEquals(date1, trip.getStarted());
		assertNull(trip.getFinished());
		assertNull(trip.getDurationSeconds());
		assertEquals(STOP_ID1, trip.getFromStopId());
		assertNull(trip.getToStopId());
		assertEquals(BigDecimal.TEN, trip.getChargeAmount());
		assertEquals(COMPANY_ID, trip.getCompanyId());
		assertEquals(BUS_ID, trip.getBusId());
		assertEquals(PAN1, trip.getPan());
		assertEquals(TransactionType.INCOMPLETE, trip.getStatus());

		assertTrue(logCaptor.getErrorLogs().isEmpty());
	}

	@Test
	public void testCreateTrip_MatchingTapFound_CompleteTrip() throws ParseException {
		TapModel tap1 =
			new TapModel(1, date1, TapType.ON, STOP_ID1, COMPANY_ID, BUS_ID, PAN1);
		TapModel tap2 =
			new TapModel(2, date2, TapType.OFF, STOP_ID2, COMPANY_ID, BUS_ID, PAN1);

		when(rateRepository.findBySourceAndDestination(STOP_ID1, STOP_ID2))
			.thenReturn(rate);

		TripModel trip = tripService.createTrip(tap1, tap2);

		verify(rateRepository).findBySourceAndDestination(STOP_ID1, STOP_ID2);

		assertNotNull(trip);
		assertEquals(date1, trip.getStarted());
		assertEquals(date2, trip.getFinished());
		assertEquals(duration, trip.getDurationSeconds());
		assertEquals(STOP_ID1, trip.getFromStopId());
		assertEquals(STOP_ID2, trip.getToStopId());
		assertEquals(BigDecimal.TEN, trip.getChargeAmount());
		assertEquals(COMPANY_ID, trip.getCompanyId());
		assertEquals(BUS_ID, trip.getBusId());
		assertEquals(PAN1, trip.getPan());
		assertEquals(TransactionType.COMPLETE, trip.getStatus());

		assertTrue(logCaptor.getErrorLogs().isEmpty());
	}

	@Test
	public void testCreateTrip_NoFutureRecordFortheSamePerson_IncompleteTrip() {

		TapModel tap1 =
			new TapModel(1, date1, TapType.ON, STOP_ID1, COMPANY_ID, BUS_ID, PAN1);
		TapModel tap2 =
			new TapModel(2, date2, TapType.OFF, STOP_ID2, COMPANY_ID, BUS_ID, PAN2);

		when(rateRepository.findFirstBySourceOrDestinationOrderByChargeDesc(STOP_ID1, STOP_ID1))
			.thenReturn(rate);

		TripModel trip = tripService.createTrip(tap1, tap2);

		verify(rateRepository).findFirstBySourceOrDestinationOrderByChargeDesc(STOP_ID1, STOP_ID1);

		assertNotNull(trip);
		assertEquals(date1, trip.getStarted());
		assertNull(trip.getFinished());
		assertNull(trip.getDurationSeconds());
		assertEquals(STOP_ID1, trip.getFromStopId());
		assertNull(trip.getToStopId());
		assertEquals(BigDecimal.TEN, trip.getChargeAmount());
		assertEquals(COMPANY_ID, trip.getCompanyId());
		assertEquals(BUS_ID, trip.getBusId());
		assertEquals(PAN1, trip.getPan());
		assertEquals(TransactionType.INCOMPLETE, trip.getStatus());

		assertTrue(logCaptor.getErrorLogs().isEmpty());
	}

	@Test
	public void testCreateTrip_MissingTapOn_IncompleteTrip() {

		TapModel tap1 =
			new TapModel(1, date1, TapType.OFF, STOP_ID1, COMPANY_ID, BUS_ID, PAN1);
		TapModel tap2 =
			new TapModel(2, date2, TapType.ON, STOP_ID2, COMPANY_ID, BUS_ID, PAN1);

		when(rateRepository.findFirstBySourceOrDestinationOrderByChargeDesc(STOP_ID1, STOP_ID1))
			.thenReturn(rate);

		TripModel trip = tripService.createTrip(tap1, tap2);

		verify(rateRepository).findFirstBySourceOrDestinationOrderByChargeDesc(STOP_ID1, STOP_ID1);

		assertNotNull(trip);
		assertEquals(date1, trip.getStarted());
		assertNull(trip.getFinished());
		assertNull(trip.getDurationSeconds());
		assertEquals(STOP_ID1, trip.getFromStopId());
		assertNull(trip.getToStopId());
		assertEquals(BigDecimal.TEN, trip.getChargeAmount());
		assertEquals(COMPANY_ID, trip.getCompanyId());
		assertEquals(BUS_ID, trip.getBusId());
		assertEquals(PAN1, trip.getPan());
		assertEquals(TransactionType.INCOMPLETE, trip.getStatus());

		assertTrue(logCaptor.getErrorLogs().isEmpty());
	}

	@Test
	public void testCreateTrip_DoubleTapOnSameStop_CancelledTrip() {

		TapModel tap1 =
			new TapModel(1, date1, TapType.ON, STOP_ID1, COMPANY_ID, BUS_ID, PAN1);
		TapModel tap2 =
			new TapModel(2, date2, TapType.OFF, STOP_ID1, COMPANY_ID, BUS_ID, PAN1);

		TripModel trip = tripService.createTrip(tap1, tap2);

		verifyNoInteractions(rateRepository);

		assertNotNull(trip);
		assertEquals(date1, trip.getStarted());
		assertEquals(date2, trip.getFinished());
		assertEquals(duration, trip.getDurationSeconds());
		assertEquals(STOP_ID1, trip.getFromStopId());
		assertEquals(STOP_ID1, trip.getToStopId());
		assertEquals(BigDecimal.ZERO, trip.getChargeAmount());
		assertEquals(COMPANY_ID, trip.getCompanyId());
		assertEquals(BUS_ID, trip.getBusId());
		assertEquals(PAN1, trip.getPan());
		assertEquals(TransactionType.CANCELLED, trip.getStatus());

		assertTrue(logCaptor.getErrorLogs().isEmpty());
	}

	@Test
	public void testCreateTrip_MissingTapOff_IncompleteTrip() {

		TapModel tap1 =
			new TapModel(1, date1, TapType.ON, STOP_ID1, COMPANY_ID, BUS_ID, PAN1);
		TapModel tap2 =
			new TapModel(2, date2, TapType.ON, STOP_ID2, COMPANY_ID, BUS_ID, PAN1);

		when(rateRepository.findFirstBySourceOrDestinationOrderByChargeDesc(STOP_ID1, STOP_ID1))
			.thenReturn(rate);

		TripModel trip = tripService.createTrip(tap1, tap2);

		verify(rateRepository).findFirstBySourceOrDestinationOrderByChargeDesc(STOP_ID1, STOP_ID1);

		assertNotNull(trip);
		assertEquals(date1, trip.getStarted());
		assertNull(trip.getFinished());
		assertNull(trip.getDurationSeconds());
		assertEquals(STOP_ID1, trip.getFromStopId());
		assertNull(trip.getToStopId());
		assertEquals(BigDecimal.TEN, trip.getChargeAmount());
		assertEquals(COMPANY_ID, trip.getCompanyId());
		assertEquals(BUS_ID, trip.getBusId());
		assertEquals(PAN1, trip.getPan());
		assertEquals(TransactionType.INCOMPLETE, trip.getStatus());

		assertTrue(logCaptor.getErrorLogs().isEmpty());
	}
}
