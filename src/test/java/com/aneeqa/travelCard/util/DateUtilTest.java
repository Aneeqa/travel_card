package com.aneeqa.travelCard.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Date;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

public class DateUtilTest {

    @Test
    public void testFindDuration_null_dates() {
        assertNull(DateUtil.findDuration(null, null));
        assertNull(DateUtil.findDuration(null, new Date()));
        assertNull(DateUtil.findDuration(new Date(), null));
    }

    @Test
    public void testFindDuration_sameDates() {
        Date date = new Date();
        assertEquals(Long.valueOf(0), DateUtil.findDuration(date, date));
    }

    @Test
    public void testFindDuration() {
        Date date1 = new Date();
        Date date2 = DateUtils.addSeconds(date1, 200);

        assertEquals(Long.valueOf("200"), DateUtil.findDuration(date1, date2));
        assertEquals(Long.valueOf("-200"), DateUtil.findDuration(date2, date1));
    }
}
