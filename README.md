This is a java application to read travel details from a csv and process the information to populate charges for each
 travel.

**System Requirement**

JDK 1.8

Maven 3.6.3 or Idea Intellij 2020

**Preparing jar**

mvn clean install

This will build the jar file and execute the tests. If you want to skip the tests, please use

mvn clean install -DskipTests

**Running**

_Intellij:_

Add -Dspring.profiles.active=local in VM options.
Right click on TravelCardApplication file and select Run 'TravelCardApplication'.

In other environment, these values will be needed
travel-card-input-file-path
travel-card-input-file-path
They can be passed with Vm options in IDE or with maven command as well.


**Database**

The application uses H2 db right now to store rates. 
On application start up the rates tabe is created when using local profile. This will not be used in production. The
 application then runs data-h2.sql file located under resources to insert rates data. 
 
 In production the database will be pre-existing with data , independent of the application.
 
**For local run:**

_The input file is located under resources : local/taps.csv
The output file: target/classes/output/trips.csv_

**Assumptions**
- All the data is present in the file and is valid.
- A person cannot continue journey between different busses of the same Company without a tap off.
- All busses travel to all the stops.
- On then On on the same stop will also be considered as CANCELLED (scenario not defined in the requirement)


**Limitations**
- Stops, company, bus are taken as string values for the scope of this task but they should be Objects.
- Application should ideally write errors while reading the input file to some output file, so that it is easy to
 detect errors. They are just logged right now.
- Unit Testing file read/write is out of scope.
- Total number of records are not too big to hold in memory in list.
- The output file path should be updated.